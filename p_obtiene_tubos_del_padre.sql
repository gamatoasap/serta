CREATE OR REPLACE PROCEDURE A001051.p_obtiene_tubos_del_padre is 
      
      reg_sprobjects  sprobjects%rowtype;
      ls_cant_tubos_exis number := 0;
      ls_estado_tubo varchar2(2):= null;
      ls_linkvalue  sprlinks.linkvalue%type;
      ls_salida varchar2(500);
      ls_nro number := 0;
       type rtdatos is record
       (objectid sprobjects.objectid%type, 
        sprid sprobjects.sprid%type, 
        projectid sprobjects.projectid%type, 
        linkid  links.linkid%type, 
        linkvalue sprlinks.linkvalue%type, 
        nro_tubos number);
      
       type  t_datos is table of rtdatos;

       tab_datos t_datos;
      
       TYPE r_catalogo IS RECORD(
        sprid  sprobjects.sprid%type,
        linkid  links.linkid%type,
        linkvalue sprlinks.linkvalue%type,
         nro_tubos number);
         
       type  t_catalogo is table of r_catalogo;

       tab_catalogo t_catalogo;
      
      cursor c is
      select b.objectid, a.sprid, a.projectid,   b.linkid, b.linkvalue,
       substr(b.linkvalue , instr (b.linkvalue, '-',2,2) +1, instr (b.linkvalue, 'T',2) - instr (b.linkvalue, '-',2,2) -1)  nro_tubos 
      from sprobjects  a, sprlinks b, links l
      where
      b.objectid = a.objectid 
      and l.linkid = b.linkid and
      b.linkid = 411 and a.sprid = 76 -----in (71,72,76) 
      and a.objectid = 108015501
      and a.logidto = 0 and b.logidto = 0;
    ----  order by 2;
      
  ---    order by 1; 
      ndato number := 0;
      begin
         ---- obtenemos que le corresponde por catalogo
        ------------guardo catalogo -----------------
      /*begin
      select distinct  a.sprid, b.linkid, b.linkvalue,
             substr(b.linkvalue , instr (b.linkvalue, '-',2,2) +1, instr (b.linkvalue, 'T',2) - instr (b.linkvalue, '-',2,2) -1)  nro_tubos
      bulk collect into tab_catalogo
      from sprobjects  a, sprlinks b, links l
      where
      b.objectid = a.objectid 
      and l.linkid = b.linkid and
      b.linkid = 411 and a.sprid = 76 ------in (71,72,76) 
      and a.logidto = 0 and b.logidto = 0;  
       
      end;
   
      
     ls_nro := tab_catalogo.count;*/
      begin    
      select b.objectid, a.sprid, a.projectid,   b.linkid, b.linkvalue,
       substr(b.linkvalue , instr (b.linkvalue, '-',2,2) +1, instr (b.linkvalue, 'T',2) - instr (b.linkvalue, '-',2,2) -1)  nro_tubos 
       bulk collect into tab_datos
      from sprobjects  a, sprlinks b, links l
      where
      b.objectid = a.objectid 
      and l.linkid = b.linkid and
      b.linkid = 411 and a.sprid = 76 -----in (71,72,76) 
     -------- and a.objectid = 108015501
      and a.logidto = 0 and b.logidto = 0 ;
      exception when others then 
      dbms_output.put_line ('erroro en procedure p obtiene tubos del padre al realizar el bulcollect ' || sqlerrm);
      end;
      
      ndato := tab_datos.count;
     for  i in 1 .. ndato loop
      ---guardo el registro 
      begin
      select *
      into reg_sprobjects 
      from sprobjects
      where objectid = tab_datos(i).objectid;
      exception when others then 
      dbms_output.put_line ('error en p obtiene tubos del padre al obtener el registro de la sprobject ' || sqlerrm);
      end;
      
      ---obtenemos la cantidad de tubos de un objeto padre
      SELECT count(*) 
             into ls_cant_tubos_exis
     FROM  sprobjects a,     SPRLiNKS b
     WHERE b.LINKVALUE =  reg_sprobjects.OBJECTNAMEID 
     and a.objectid = b.objectid
     and b.linkid in (242) 
     and a.sprid = 154
     and a.logidto = 0 and b.logidto = 0;
  
     
     IF nvl(tab_datos(I).NRO_TUBOS,0) <> nvl( ls_cant_tubos_exis,0) THEN
        ls_salida := ' para el objecto ' || tab_datos(I).objectid || ' con sprid ' || reg_sprobjects.sprid || ' la cantidad de tubos que tiene ' || ls_cant_tubos_exis || '  no es la correcta del catalogo, la cual es ' || tab_datos(I).nro_tubos ;
      elsif nvl(tab_datos(I).NRO_TUBOS,0) = nvl( ls_cant_tubos_exis,0) THEN
        ls_salida := ' para el objecto ' || tab_datos(i).objectid || ' con sprid ' || reg_sprobjects.sprid || ' tiene la cantidad correcta de tubos ' || ls_cant_tubos_exis || ' correspondiente al elemento del catalogo ' || tab_datos(I).linkvalue;    
      end if; 
      insert into p_salida values (ls_salida );
      commit;
     end loop;
   /*    for j in 1 .. ls_nro  loop
          if nvl( tab_catalogo(j).nro_tubos,0) = nvl(ls_cant_tubos_exis,0) then
            ls_estado_tubo := 'OK';
            ls_linkvalue := tab_catalogo(j).linkvalue;
          end if;   
       end loop; */
     
      
     ------------------------------
    /* if nvl(ls_estado_tubo,'NOK')  = 'OK' then
     ---  dbms_output.put_line (' para el objecto ' || i.objectid || ' con sprid ' || reg_sprobjects.sprid || ' tiene la cantidad correcta de tubos ' || ls_cant_tubos_exis || 'correspondiente al elemento del catalogo ' || ls_linkvalue );
       ls_salida := ' para el objecto ' || i.objectid || ' con sprid ' || reg_sprobjects.sprid || ' tiene la cantidad correcta de tubos ' || ls_cant_tubos_exis || 'correspondiente al elemento del catalogo ' || ls_linkvalue;    
        insert into p_salida values (ls_salida );
     
    else --no se encotro
    ls_salida := ' para el objecto ' || i.objectid || ' con sprid ' || reg_sprobjects.sprid || 'la cantidad de tubos que tiene ' || ls_cant_tubos_exis || '  no es la correcta del catalogo';
     --- dbms_output.put_line (' para el objecto ' || i.objectid || ' con sprid ' || reg_sprobjects.sprid || 'la cantidad de tubos que tiene ' || ls_cant_tubos_exis || '  no es la correcta del catalogo' );
    insert into p_salida values (ls_salida );
     end if;*/ 

 
     exception when others then
       dbms_output.put_line ('el proceso termino NOK ' || sqlerrm);
     end;
/